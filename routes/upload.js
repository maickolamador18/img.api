const multer = require("multer")
const express = require('express')
const app = express.Router()
const path = require('path')
const { v4: uuidv4 } = require('uuid')
const sharp = require('sharp');


//IMPORTANT GLOBAL NAME
let trueId = uuidv4()
let lastId = trueId

//MULTER CONFIG
const storageName = multer.diskStorage({
  destination: path.join(__dirname+`/../public/`),
  filename:(req,file, cb)=>{
    cb(null, trueId)
  }
})

//RESIZE FUNCTION
const convertImage =(whatFile, id, type,size,label)=>{
  setTimeout(()=>{
    sharp(whatFile)
    .resize(size.w,size.h,{
      fit:"fill"
    })
    .toFile(path.join(__dirname+`/../public/${id}-${label+type}`))
    .then(data => console.log(`${id} codec to ${type}`))
    .catch(err => console.log(err))
    console.log(whatFile)
  },1000)
}

//HANDLING FILE
app.use(multer({
  storage:storageName,
  dest:path.join(__dirname+`/../public/`),
  limits:{
    fileSize:10000000
  },
  fileFilter:(req,file,cb)=>{
    const fileTypes = /jpeg|jpg|png|gif|webp/
    const mimeType = fileTypes.test(file.mimetype)
    const extName = fileTypes.test(path.extname(file.originalname))

    if (mimeType && extName) {
      
      trueId = trueId+path.extname(file.originalname)
      
      const pathId = path.join(__dirname+`/../public/${trueId}`)
          //SAVING MULTIPLE TYPES OF IMAGES
      convertImage(pathId,lastId,".webp",{w:117,h:176},"mini")
      convertImage(pathId,lastId,".jpg",{w:117,h:176},"mini")

      //BY FACTOR OF 2
      convertImage(pathId,lastId,".jpg",{w:234,h:352},"medium")
      convertImage(pathId,lastId,".webp",{w:234,h:352},"medium")

      convertImage(pathId,lastId,".jpg",{w:468,h:704},"high")
      convertImage(pathId,lastId,".webp",{w:468,h:704},"high")

      convertImage(pathId,lastId,".jpg",{w:936,h:1408},"ultraHigh")
      convertImage(pathId,lastId,".webp",{w:936,h:1408},"ultraHigh")
      
      return cb(null, true)
    }
    cb(`{msg:"El archivo debe ser una imagen valida"}`)
  }
}).single('contentImage'))


//ENDPOINT
app.post('/',(req,res)=>{
  
  res.json({msg:lastId})
})

module.exports = app