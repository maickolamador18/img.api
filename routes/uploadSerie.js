const express = require('express')
const router = express.Router()
const upload = require('../models/uploadSerieModel')
const uniqid = require("uniqid")



const joi = require('@hapi/joi')

const publicarSerieValidator = (objToValid)=>{
    const Schema = joi.object({
        uniqid: joi.string().required(),
        dueño: joi.string().min(3).max(64).required(),
        tipo: joi.string().max(32).required(),
        estado: joi.string().max(32).required(),
        titulo: joi.string().max(16000).required(),
        img: joi.string().max(256).required(),
        pais: joi.string().max(64).required(),
        audio: joi.string().max(64).required(),
        año: joi.number().required(),
        categorias: joi.array().max(64).required(),
        descripcion: joi.string().required()
    })

    //VALIDATING
    return Schema.validate(objToValid)
    
}


//SUBMITING A DRAMA
router.post('/', async (req,res)=>{
    req.body.uniqid = uniqid()+"-"+req.body.titulo

    const newSerie = new upload(req.body)

    const saveData = await newSerie.save()

    
    res.send(publicarSerieValidator(req.body))
})


module.exports = router