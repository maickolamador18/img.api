//INIT SERVER
const express = require('express')
const app = express()
const path = require('path')
//DB
const mongoose = require('mongoose')
require('dotenv/config')

//CORS
app.use((req, res, next)=> {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
});

//PARSING
const bodyParser = require('body-parser')

app.use(bodyParser.json())


//RESQUEST

//IMAGES
const uploadImage = require('./routes/upload')
app.use('/upload/image', uploadImage)

//CONTENT API HANDLER
const uploadSerie = require('./routes/uploadSerie')

app.use('/upload/serie', uploadSerie)


//DEV
app.get('/',(req,res)=>{
    res.json({msg:"nothing to see here, maybe you are lost"})
}) 


//PUBLIC IMG
app.use(express.static(path.join(__dirname+'/public')))


mongoose.connect(
    process.env.DB_CONNECTION,
    {useNewUrlParser:true,
     useUnifiedTopology:true},
    ()=>{
        console.log("connected")
    }
)

const port = process.env.PORT || 4000


//START SERVER
app.listen(port)