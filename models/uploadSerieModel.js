const mongoose = require('mongoose')

const contenidoSchema = mongoose.Schema({
    dueño:{
        type:String,
        required:true
    },
    uniqid: {
        type: String,
        required: true
    },
    tipo: {
        type: String,
        required: true
    },
    estado: {
        type: String,
        required: true
    },
    titulo: {
        type: String,
        required: true
    },
    descripcion: {
        type: String,
        required: true
    },
    img: {
        type: String,
        required: true
    },
    pais: {
        type: String,
        required: true
    },
    audio: {
        type: String,
        required: true
    },
    año: {
        type: Number,
        required: true
    },
    categorias: {
        type: Array,
        required: true
    },
    temporadas:{
        type:Array,
        required:false
    },
    actores:{
        type:Array,
        required: false
    },
    fecha: {
        type: Date,
        default: Date.now
    }
})


module.exports = mongoose.model('contenido', contenidoSchema)